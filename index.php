<?php get_header(); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div id="content-box">

			<div class="row">
				<div class="col-md-8 content article blog">

<ol class="breadcrumb">
  <li><a href="http://www.homescentralcoast.com/">Home</a></li>
  <li class="active">Blog</li>
</ol>

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'content' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				//comments_template();
			endif;

		// End the loop.
		endwhile;
		?>


		</div>
		<div class="col-md-4">
			<?php get_sidebar(); ?>
		</div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
