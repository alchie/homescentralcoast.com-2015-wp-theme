<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head profile="http://www.w3.org/2005/10/profile">
<link rel="icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/favicon.png">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<script>(function(){document.documentElement.className='js'})();</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

 <body>
    <div id="wrap">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div id="header"> 
						<div class="row">
							<div class="col-md-5">
								<div class="branding">
									<h1 id="site-title"><a href="http://www.homescentralcoast.com/">Homes Central Coast</a></h1>
								</div>
							</div>
							<div class="col-md-7">
								<div class="navigation">
									<nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <div class="navbar-brand visible-xs" href="/">
	<span class="glyphicon glyphicon-envelope"></span> <a href="http://www.homescentralcoast.com/inquiry.html" class="">Contact us</a> <span class="phone"><span class="glyphicon glyphicon-earphone"></span> <span class="number">805.937.1764</span></span></div>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
        <li><a href="http://propertysearch.homescentralcoast.com/idx/search/advanced">Property Search</a></li>
		<li><a href="http://www.homescentralcoast.com/find_home_values.php">Sell A Home</a></li>
		<li><a href="http://www.homescentralcoast.com/cities_and_lifestyles.php">Relocating</a></li>
		<li><a href="http://www.homescentralcoast.com/about.html">About</a></li>
		<li><a href="http://www.homescentralcoast.com/real-estate-blog/">Blog</a></li>
    </ul>
    </div><!-- /.navbar-collapse -->
</nav>
								</div>
							</div>
						</div>
						<div class="top_contacts muli hidden-xs">
							<span class="glyphicon glyphicon-envelope"></span> <a href="http://www.homescentralcoast.com/inquiry.html">Contact us</a> or call: <span class="phone"><span class="glyphicon glyphicon-earphone"></span> <span class="number">805.937.1764</span></span>
						</div>
					</div>
				</div>
			</div>
		</div>
