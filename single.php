<?php get_header(); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div id="content-box">
						
			<div class="row">
				<div class="col-md-8 content article blog">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

		?>
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="http://www.homescentralcoast.com/real-estate-blog/">Blog</a></li>
  <li class="active"><?php the_title(); ?></li>
</ol>
		<?php 
			// Include the page content template.
			get_template_part( 'content', 'single' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				//comments_template();
			endif;

		// End the loop.
		endwhile;
		?>


		</div>
		<div class="col-md-4">
			<?php get_sidebar(); ?>
		</div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
