	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<label for="s" class="assistive-text hidden"><?php _e( 'Search', 'hcc2015' ); ?></label>
		<div class="form-group">
		<input type="text" class="field form-control" name="s" id="s" placeholder="<?php esc_attr_e( 'Search', 'hcc2015' ); ?>" />
		
		<input type="submit" class="submit hidden" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'hcc2015' ); ?>" />
		</div>
	</form>
