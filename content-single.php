
		
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="page-header">
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
		<div class="entry-meta">
			<?php hcc2015_entry_meta(); ?>
			<?php edit_post_link( __( 'Edit', 'twentythirteen' ), ' | <span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-meta -->
	</header> 

	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->
	
</article><!-- #post-## -->
		
<hr>
