	</div>
	<div id="footer">
		<div class="container">
			<div class="row navigation">
				<div class="col-md-12">
					<ul class="nav nav-pills">
						<li><a href="http://www.homescentralcoast.com/">Home</a></li>
						<li><a href="http://propertysearch.homescentralcoast.com/idx/search/advanced">Property Search</a></li>
						<li><a href="http://www.homescentralcoast.com/site-map.html">Site Map</a></li>
						<li><a href="http://www.homescentralcoast.com/foreclosures.html">Foreclosures</a></li>
						<li><a href="http://www.homescentralcoast.com/new_listing.html">New Listings</a></li>
						<li><a href="http://www.homescentralcoast.com/real-estate-blog/">Blog</a></li>
						<li><a href="http://www.homescentralcoast.com/about.html">About Us</a></li>
					</ul>
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="row widgets">
				<div class="col-md-4 widget-box">
					<h4 class="widget-title">Contact Joanna Stanfield:</h4>
					<span>BRE: 00924374</span>
					<p class="phone muli"><a href="tel:8059371764">805.937.1764</a></p>
					<p><small>CONNECT WITH US:</small></p>
					<ul class="social-media">
						<li class="facebook"><a href="http://www.facebook.com/pages/Homes-Central-Coast-Real-Estate/221701307853900?sk=app_2373072738">Facebook</a></li>
						<li class="twitter"><a href="http://twitter.com/#!/HCCRealEstate">Twitter</a></li>
						<li class="youtube"><a href="http://www.youtube.com/user/HomesCentralCoast">YouTube</a></li>
						<li class="linkedin"><a href="http://www.linkedin.com/in/eddiestanfield">LinkedIn</a></li>
						<li class="google"><a href="">Google+</a></li>
					</ul>
				</div>
				<div class="col-md-4 widget-box">
					<h4 class="widget-title">Testimonials</h4>
					<p>"Joanna was pro-active, innovative, and creative in her marketing of our Condo. It was important to me to have someone who did a great job of communicating with me, and she was exactly what I had hoped for in an agent. I highly recommend her for any of your Real Estate needs!!!"</p> 
<em>Sheryl M.</em>
				</div>
				<div class="col-md-4 widget-box">
					<h4 class="widget-title">Recent Blog Post</h4>
<?php $latest = get_posts(); ?>					
					<h5><?php echo $latest[0]->post_title; ?></h5>
<p><?php echo ($latest[0]->post_excerpt=='')? substr( strip_tags($latest[0]->post_content), 0, 160) : $latest[0]->post_excerpt; ?></p>
<strong><a href="<?php echo get_permalink($latest[0]->ID); ?>">READ MORE <span class="glyphicon glyphicon-play link-arrow"></span></a></strong>
				</div>
			</div>
			
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<a href="#">Equal Housing Opportunity</a> | <a href="http://www.homescentralcoast.com/privacy_policy.html">Privacy Policy</a>
					<p>All content &copy; Homes Central Coast. All Rights Reserved</p>
				</div>
				<div class="col-md-8">
					<p class="text-right">1160 Price Street, Pismo Beach, CA 93449 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1811 Broadway, Santa Maria, CA 9345 <br>
                 599 Higuera Street, San Luis Obispo, CA  93401 </p>
					
				</div>
			</div>
		</div>
	</div>
<?php wp_footer(); ?>

</body>
</html>
