var verifyFindHomeValues = function() {
	if( jQuery('#form_first_name').val() == '') {
		alert("The First Name field is required.");
		jQuery('#form_first_name').focus();
		return false;
	}
	if( jQuery('#form_last_name').val() == '') {
		alert("The Last Name field is required.");
		jQuery('#form_last_name').focus();
		return false;
	}
	if( jQuery('#form_email').val() == '') {
		alert("The Email field is required.");
		jQuery('#form_email').focus();
		return false;
	} else {
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test( jQuery('#form_email').val() ) == false) {
			alert("Invalid E-mail Address! Please re-enter.");
			jQuery('#form_email').focus();
			return false;
		}
	}
	if( jQuery('#form_phone').val() == '') {
		alert("The Phone field is required.");
		jQuery('#form_phone').focus();
		return false;
	}
	return true;
};

var verifySidebarRelocating = function() {
	if( jQuery('#sidebar_relocating_form_name').val() == '') {
		alert("The Name field is required.");
		jQuery('#sidebar_relocating_form_name').focus();
		return false;
	}
	if( jQuery('#sidebar_relocating_form_email').val() == '') {
		alert("The Email field is required.");
		jQuery('#sidebar_relocating_form_email').focus();
		return false;
	} else {
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test( jQuery('#sidebar_relocating_form_email').val() ) == false) {
			alert("Invalid E-mail Address! Please re-enter.");
			jQuery('#sidebar_relocating_form_email').focus();
			return false;
		}
	}
	return true;
};

var verifySidebarRelocating2 = function() {
	if( jQuery('#sidebar_relocating_form_email').val() == '') {
		alert("The Email field is required.");
		jQuery('#sidebar_relocating_form_email').focus();
		return false;
	} else {
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test( jQuery('#sidebar_relocating_form_email').val() ) == false) {
			alert("Invalid E-mail Address! Please re-enter.");
			jQuery('#sidebar_relocating_form_email').focus();
			return false;
		}
	}
	return true;
};

var verifyNewListings = function() {
	if( jQuery('#form_email').val() == '') {
		alert("The Email field is required.");
		jQuery('#form_email').focus();
		return false;
	} else {
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test( jQuery('#form_email').val() ) == false) {
			alert("Invalid E-mail Address! Please re-enter.");
			jQuery('#form_email').focus();
			return false;
		}
	}
	if( jQuery('#form_first_name').val() == '') {
		alert("The First Name field is required.");
		jQuery('#form_first_name').focus();
		return false;
	}
	
	if(
	(jQuery('#form_hometype_1').prop('checked') == false) 
	&& (jQuery('#form_hometype_2').prop('checked') == false) 
	&& (jQuery('#form_hometype_3').prop('checked') == false) 
	){
		alert("Select a type of home.");
		return false;
	}
	
	if( jQuery('#form_cities').val() == '') {
		alert("Select at least One (1) City");
		return false;
	}
	
	return false;
};


jQuery('#form_citySource option').dblclick(function(){
	if( $(this).attr('data-added') == '1' ) {
		$(this).attr('data-added', '');
		jQuery('#form_citySource').append( $(this) );
	} else {
		$(this).attr('data-added', 1);
		jQuery('#form_cityDestination').append( $(this) );
	}
	console.log( $(this) );
});

jQuery('.form_agent').click(function(){
	if( $(this).val() == 'YES' ) {
		$('#form_agents_name').removeClass('hidden');
	} else {
		$('#form_agents_name').addClass('hidden');
	}
});

jQuery('.add-cities .btn-add').click(function(){
	alert('test');
});



