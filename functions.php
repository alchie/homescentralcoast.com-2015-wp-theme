<?php

require('inc/bootstrap3-menu-walker.php');

if ( ! function_exists( 'payodacorp_setup' ) ) :

function hcc2015_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on hcc2015, use a find and replace
	 * to change 'hcc2015' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'hcc2015', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'hcc2015' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	/* add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) ); */

}
endif; // hcc2015_setup
add_action( 'after_setup_theme', 'hcc2015_setup' );

function hcc2015_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'hcc2015' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'hcc2015' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s panel panel-default"><div class="panel-heading">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h3 class="widget-title panel-title">',
		'after_title'   => '</h3></div><div class="panel-body">',
	) );
}
add_action( 'widgets_init', 'hcc2015_widgets_init' );

function hcc2015_scripts() {

	wp_enqueue_style( 'Muli-font', 'http://fonts.googleapis.com/css?family=Muli:300,400');
	
	// Add Bootstrap, used in the main stylesheet.
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.1.0' );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '20150601' );
	//wp_enqueue_style( 'bootstrap-cdn-css', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css', array(), '3.3.2' );
	//wp_enqueue_script( 'bootstrap-cdn-js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js', array(), '20150210' );
		
	// WOW  Animation
	//wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/css/animate.css', array(), '1.0' );
	//wp_enqueue_script( 'wow-min-js', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), '1.0.3' );
	
	// Load our main stylesheet.
	wp_enqueue_style( 'hcc2015-styles', get_template_directory_uri() . '/css/styles.css', array('bootstrap-css'), '20150601' );
	wp_enqueue_style( 'hcc2015-media', get_template_directory_uri() . '/css/media.css', array('bootstrap-css', 'hcc2015-styles'), '20150601' );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'hcc2015-ie', get_template_directory_uri() . '/css/ie.css', array( 'hcc2015-style' ), '20141010' );
	wp_style_add_data( 'hcc2015-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'hcc2015-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'hcc2015-style' ), '20141010' );
	wp_style_add_data( 'hcc2015-ie7', 'conditional', 'lt IE 8' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'hcc2015-script', get_template_directory_uri() . '/js/scripts.js', array( 'jquery', 'bootstrap-js' ), '20150506.2', true );

}
add_action( 'wp_enqueue_scripts', 'hcc2015_scripts' );


function hcc2015_entry_meta() {
	if ( is_sticky() && is_home() && ! is_paged() )
		echo '<span class="featured-post">' . __( 'Sticky', 'hcc2015' ) . '</span>';

	if ( ! has_post_format( 'link' ) && 'post' == get_post_type() )
		hcc2015_entry_date();

	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'hcc2015' ) );
	if ( $categories_list ) {
		echo ' | <span class="categories-links">' . $categories_list . '</span>';
	}

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'hcc2015' ) );
	if ( $tag_list ) {
		echo ' | <span class="tags-links">' . $tag_list . '</span>';
	}

	// Post author
	if ( 'post' == get_post_type() ) {
		printf( ' | <span class="author vcard">Posted By <a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'hcc2015' ), get_the_author() ) ),
			get_the_author()
		);
	}
}

function hcc2015_entry_date( $echo = true ) {
	if ( has_post_format( array( 'chat', 'status' ) ) )
		$format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'twentythirteen' );
	else
		$format_prefix = '%2$s';

	$date = sprintf( '<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( sprintf( __( 'Permalink to %s', 'twentythirteen' ), the_title_attribute( 'echo=0' ) ) ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
	);

	if ( $echo )
		echo $date;

	return $date;
}

add_action( 'wp_ajax_recent_post', 'hcc2015_recent_post' );
add_action( 'wp_ajax_nopriv_recent_post', 'hcc2015_recent_post' );
function hcc2015_recent_post() {
	header("Access-Control-Allow-Origin: *"); 
	$latest = get_posts( array(
		'posts_per_page'   => 1
	));
	
	$excerpt = $latest[0]->post_excerpt;
	if( $excerpt == '') {
		$excerpt = substr( strip_tags($latest[0]->post_content), 0, 160);
	}
	
	echo json_encode( array(
		'title' => $latest[0]->post_title,
		'link' => get_the_permalink($latest[0]->ID),
		'content' => $excerpt
	) );
	wp_die();
}